﻿using System;
using System.Collections.Generic;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using St.OneSysTestWebApi.Controllers;
using St.OneSysTestWebApi.DomainModel.Models;
using St.OneSysTestWebApi.Models;
using St.OneSysTestWebApi.Tests.DALFake;

namespace St.OneSysTestWebApi.Tests.Controllers
{
    [TestClass]
    public class ReportsControllerTest
    {
        [TestMethod]
        public void ReportTotal()
        {
            var dbFake = new DataBaseFake
            {
                Customers = new List<Customer>
                {
                    new Customer {Id = 1, Name = "Клиент 1", Email = "email1@mail.com"},
                    new Customer {Id = 2, Name = "Клиент 2", Email = "email2@mail.com"},
                    new Customer {Id = 3, Name = "Клиент 3", Email = "email3@mail.com"}
                },
                Orders = new List<Order>
                {
                    new Order {Id = 1, CustomerId = 1, Price = 1000m, CreatedDate = new DateTime(2016, 8, 1, 15, 20, 00)},
                    new Order {Id = 2, CustomerId = 2, Price = 2000m, CreatedDate = new DateTime(2016, 8, 2, 16, 00, 00)},
                    new Order {Id = 3, CustomerId = 2, Price = 4000m, CreatedDate = new DateTime(2016, 8, 3, 17, 10, 00)},
                    new Order {Id = 4, CustomerId = 3, Price = 8000m, CreatedDate = new DateTime(2016, 8, 4, 18, 10, 00)},
                    new Order {Id = 5, CustomerId = 3, Price = 16000m, CreatedDate = new DateTime(2016, 8, 5, 18, 10, 00)},
                    new Order {Id = 6, CustomerId = 3, Price = 32000m, CreatedDate = new DateTime(2016, 8, 6, 18, 10, 00)}
                }
            };

            var uow = new UnitOfWorkFake(dbFake);
            var controller = new ReportsController(uow);

            var customersCountBefore = dbFake.Customers.Count;
            var ordersCountBefore = dbFake.Orders.Count;


            var result = controller.GetTotal() as OkNegotiatedContentResult<ReportApiModel>;

            Assert.IsNotNull(result);

            Assert.AreEqual(customersCountBefore, result.Content.CustomerCount);
            Assert.AreEqual(ordersCountBefore, result.Content.OrderCount);
            Assert.AreEqual(10500, result.Content.OrderPriceAverage);
            Assert.AreEqual(2, result.Content.OrdersOfCustomerAverageCount);
        }
    }
}