﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using St.OneSysTestWebApi.Controllers;
using St.OneSysTestWebApi.DomainModel.Models;
using St.OneSysTestWebApi.Models;
using St.OneSysTestWebApi.Tests.DALFake;

namespace St.OneSysTestWebApi.Tests.Controllers
{
    [TestClass]
    public class CustomersControllerTest
    {
        protected const string TestCustomerName = "Клиент 2";
        protected const int TestCustomerId = 2;

        protected DataBaseFake GetDataBaseFake()
        {
            return new DataBaseFake
            {
                Customers = new List<Customer>
                {
                    new Customer {Id = 1, Name = "Клиент 1", Email = "email1@mail.com"},
                    new Customer {Id = TestCustomerId, Name = TestCustomerName, Email = "email2@mail.com"},
                    new Customer {Id = 3, Name = "Клиент 3", Email = "email3@mail.com"}
                },
                Orders = new List<Order>
                {
                    new Order {Id = 1, CustomerId = 1, Price = 1000m, CreatedDate = new DateTime(2016, 8, 1, 15, 20, 00)},
                    new Order {Id = 2, CustomerId = 2, Price = 2000m, CreatedDate = new DateTime(2016, 8, 2, 16, 00, 00)},
                    new Order {Id = 3, CustomerId = 2, Price = 4000m, CreatedDate = new DateTime(2016, 8, 3, 17, 10, 00)},
                    new Order {Id = 4, CustomerId = 3, Price = 8000m, CreatedDate = new DateTime(2016, 8, 4, 18, 10, 00)}
                }
            };
        }


        [TestMethod]
        public void GetAllCustomers()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var result = controller.Get() as OkNegotiatedContentResult<List<CustomerApiModel>>;

            Assert.IsNotNull(result);
            Assert.AreEqual(dbFake.Customers.Count, result.Content.Count);
        }

        [TestMethod]
        public void GetOrdersOfCustomers_CustomerExist()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var result = controller.Get(TestCustomerId);

            var resultAsNotFound = result as NotFoundResult;

            Assert.IsNull(resultAsNotFound);

            var resultAsOk = result as OkNegotiatedContentResult<CustomerApiModel>;

            Assert.IsNotNull(resultAsOk);
            Assert.AreEqual(TestCustomerName, resultAsOk.Content.Name);
            Assert.IsNotNull(resultAsOk.Content.Orders);
            Assert.AreEqual(dbFake.Orders.Count(o => o.CustomerId == TestCustomerId), resultAsOk.Content.Orders.Count);
            Assert.AreEqual(dbFake.Orders.Where(o => o.CustomerId == TestCustomerId).Sum(p => p.Price), resultAsOk.Content.Orders.Sum(o => o.Price));
        }

        [TestMethod]
        public void GetOrdersOfCustomers_CustomerNotExist()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var result = controller.Get(50) as NotFoundResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PostCustomer_ModelIsValid()
        {
            var newCustomerName = "Новый Клиент 4";

            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var customersCountBefore = dbFake.Customers.Count;

            var newCustomer = new CustomerApiModel
            {
                Name = newCustomerName,
                Email = "email4@mail.com"
            };

            controller.Configuration = new HttpConfiguration();
            controller.Validate(newCustomer);

            var result = controller.Post(newCustomer);

            var resultAsBadRequest = result as BadRequestResult;

            Assert.IsNull(resultAsBadRequest);

            var resultAsOk = result as StatusCodeResult;

            Assert.IsNotNull(resultAsOk);
            Assert.AreEqual(HttpStatusCode.Created, resultAsOk.StatusCode);
            Assert.IsTrue(uow.DataBaseFake.Customers.Any(c => c.Name == newCustomerName));

            Assert.AreEqual(customersCountBefore + 1, uow.DataBaseFake.Customers.Count);
        }


        [TestMethod]
        public void PostCustomer_ModelIsNotValid()
        {
            var newCustomerName = "Новый Клиент 4";

            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var customersCountBefore = dbFake.Customers.Count;

            var newCustomer = new CustomerApiModel
            {
                Name = newCustomerName,
                Email = ""
            };

            controller.Configuration = new HttpConfiguration();
            controller.Validate(newCustomer);

            var result = controller.Post(newCustomer);

            var resultAsBadRequest = result as InvalidModelStateResult;

            Assert.IsNotNull(resultAsBadRequest);
            Assert.IsFalse(resultAsBadRequest.ModelState.IsValidField("Email"));

            Assert.AreEqual(customersCountBefore, uow.DataBaseFake.Customers.Count);
        }

        [TestMethod]
        public void PostCustomer_ModelIsNull()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var customersCountBefore = dbFake.Customers.Count;

            var result = controller.Post(null);

            var resultAsBadRequest = result as BadRequestResult;

            Assert.IsNotNull(resultAsBadRequest);

            Assert.AreEqual(customersCountBefore, uow.DataBaseFake.Customers.Count);
        }

        [TestMethod]
        public void PostOrder_ModelIsValid()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var ordersOfCustomerCountBefore = dbFake.Orders.Count(p => p.CustomerId == TestCustomerId);

            var newOrder = new OrderApiModel
            {
                Price = 16000m,
                CreatedDate = new DateTime(2016, 8, 5, 20, 10, 00)
            };

            controller.Configuration = new HttpConfiguration();
            controller.Validate(newOrder);

            var result = controller.Post(TestCustomerId, newOrder);

            var resultAsBadRequest = result as BadRequestResult;

            Assert.IsNull(resultAsBadRequest);

            var resultAsOk = result as StatusCodeResult;

            Assert.IsNotNull(resultAsOk);
            Assert.AreEqual(HttpStatusCode.Created, resultAsOk.StatusCode);

            Assert.AreEqual(ordersOfCustomerCountBefore + 1, uow.DataBaseFake.Orders.Count(p => p.CustomerId == TestCustomerId));
        }

        [TestMethod]
        public void PostOrder_ModelIsValidAndCustomerNotExist()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var ordersOfCustomerCountBefore = dbFake.Orders.Count(p => p.CustomerId == TestCustomerId);

            var newOrder = new OrderApiModel
            {
                Price = 16000m,
                CreatedDate = new DateTime(2016, 8, 5, 20, 10, 00)
            };

            controller.Configuration = new HttpConfiguration();
            controller.Validate(newOrder);

            var result = controller.Post(1000, newOrder);

            var resultNotFound = result as NotFoundResult;
            Assert.IsNotNull(resultNotFound);

            Assert.AreEqual(ordersOfCustomerCountBefore, uow.DataBaseFake.Orders.Count(p => p.CustomerId == TestCustomerId));
        }

        [TestMethod]
        public void PostOrder_ModelIsValidWithoutCreatedDate()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var ordersOfCustomerCountBefore = dbFake.Orders.Count(p => p.CustomerId == TestCustomerId);

            var newOrder = new OrderApiModel
            {
                Price = 16000m
            };

            controller.Configuration = new HttpConfiguration();
            controller.Validate(newOrder);

            var result = controller.Post(TestCustomerId, newOrder);

            var resultAsBadRequest = result as BadRequestResult;

            Assert.IsNull(resultAsBadRequest);

            var resultAsOk = result as StatusCodeResult;

            Assert.IsNotNull(resultAsOk);
            Assert.AreEqual(HttpStatusCode.Created, resultAsOk.StatusCode);

            Assert.AreEqual(ordersOfCustomerCountBefore + 1, uow.DataBaseFake.Orders.Count(p => p.CustomerId == TestCustomerId));
        }

        [TestMethod]
        public void PostOrder_ModelIsNotValid()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var orderCountBefore = dbFake.Orders.Count;

            //проверка случая неверного формата даты
            DateTime createdDate;
            DateTime.TryParse("2016 05 20 20", out createdDate); 

            var newOrder = new OrderApiModel
            {
                Price = 16000m,
                CreatedDate = createdDate
            };

            controller.Configuration = new HttpConfiguration();
            controller.Validate(newOrder);
            controller.ModelState.AddModelError("CreatedDate", "CreatedDate is not valid");

            var result = controller.Post(TestCustomerId, newOrder);

            var resultAsBadRequest = result as InvalidModelStateResult;

            Assert.IsNotNull(resultAsBadRequest);
            Assert.IsFalse(resultAsBadRequest.ModelState.IsValidField("CreatedDate"));

            Assert.AreEqual(orderCountBefore, dbFake.Orders.Count);
        }

        [TestMethod]
        public void PostOrder_ModelIsNull()
        {
            var dbFake = GetDataBaseFake();
            var uow = new UnitOfWorkFake(dbFake);
            var controller = new CustomersController(uow);

            var orderCountBefore = dbFake.Orders.Count;


            var result = controller.Post(TestCustomerId, null);

            var resultAsBadRequest = result as BadRequestResult;

            Assert.IsNotNull(resultAsBadRequest);

            Assert.AreEqual(orderCountBefore, dbFake.Orders.Count);
        }
    }
}