﻿using St.OneSysTestWebApi.DomainModel.DAL;
using St.OneSysTestWebApi.DomainModel.Models;

namespace St.OneSysTestWebApi.Tests.DALFake
{
    public class UnitOfWorkFake : IUnitOfWork
    {
        public UnitOfWorkFake(DataBaseFake dataBaseFake)
        {
            DataBaseFake = DataBaseFakeTemp = dataBaseFake;
        }

        public DataBaseFake DataBaseFake;

        public DataBaseFake DataBaseFakeTemp;

        private IGenericRepository<Customer> _customerRepository;
        public IGenericRepository<Customer> CustomerRepository => _customerRepository ?? (_customerRepository = new GenericRepositoryFake<Customer>(DataBaseFakeTemp.Customers));


        private IGenericRepository<Order> _orderRepository;
        public IGenericRepository<Order> OrderRepository => _orderRepository ?? (_orderRepository = new GenericRepositoryFake<Order>(DataBaseFakeTemp.Orders));
        public void Save()
        {
            DataBaseFake = DataBaseFakeTemp;
        }
    }
}
