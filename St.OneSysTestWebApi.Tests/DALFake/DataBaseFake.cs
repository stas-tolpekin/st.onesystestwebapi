using System.Collections.Generic;
using St.OneSysTestWebApi.DomainModel.Models;

namespace St.OneSysTestWebApi.Tests.DALFake
{
    public class DataBaseFake
    {
        public List<Customer> Customers { get; set; }
        public List<Order> Orders { get; set; } 
    }
}