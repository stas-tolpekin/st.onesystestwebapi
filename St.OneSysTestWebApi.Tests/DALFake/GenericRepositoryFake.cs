﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using St.OneSysTestWebApi.DomainModel.DAL;
using St.OneSysTestWebApi.DomainModel.Models;

namespace St.OneSysTestWebApi.Tests.DALFake
{
    public class GenericRepositoryFake<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected List<T> List;

        public GenericRepositoryFake(List<T> list)
        {
            List = list;
        }


        public void Delete(T entityToDelete)
        {
            List.Remove(entityToDelete);
        }


        public IQueryable<T> GetQuery(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            var listQuery = List.AsQueryable();

            if (filter != null)
                listQuery = listQuery.Where(filter);

            if (orderBy != null)
                return orderBy(listQuery);

            return listQuery;
        }

        public void Insert(T entity)
        {
            var nextIndex = List.Any() ? List.Max(i => i.Id) + 1 : 1;
            entity.Id = nextIndex;

            List.Add(entity);
        }

        public void Update(T entityToUpdate)
        {
            var item = List.FirstOrDefault(p => p.Id == entityToUpdate.Id);
            if (item == null) throw new KeyNotFoundException();

            var itemIndex = List.IndexOf(item);
            List.RemoveAt(itemIndex);
            List.Insert(itemIndex, entityToUpdate);
        }
    }
}