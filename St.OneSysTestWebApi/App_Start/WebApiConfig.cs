﻿using System.Web.Http;
using Newtonsoft.Json.Serialization;

namespace St.OneSysTestWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            
            
        }

        public static void ConfigureApi(HttpConfiguration config)
        {
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //Отключение XmlFormatter. В этом случае WebApi перестанет слушаться заголовка Accept:application/xml.
            //config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}
