﻿using System;
using System.Linq;
using System.Web.Http;
using St.OneSysTestWebApi.Controllers.Base;
using St.OneSysTestWebApi.DomainModel.DAL;
using St.OneSysTestWebApi.Models;

namespace St.OneSysTestWebApi.Controllers
{
    public class ReportsController : BaseApiController
    {
        public ReportsController()
        {
        }

        public ReportsController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        [Route("api/reports/total")]
        public IHttpActionResult GetTotal()
        {
            var orders = UnitOfWork.OrderRepository.GetQuery()
                .Select(o => new
                {
                    o.CustomerId,
                    o.Price
                })
                .ToList();

            var customerIds = UnitOfWork.CustomerRepository.GetQuery()
                .Select(c => c.Id)
                .ToList();

            var ordersOfCustomerAverage = customerIds
                .Select(cId => orders.Count(o => o.CustomerId == cId))
                .Average();

            var priceAverage = orders.Average(o => o.Price);

            var report = new ReportApiModel
            {
                CustomerCount = customerIds.Count,
                OrderCount = orders.Count,
                OrdersOfCustomerAverageCount = Math.Round(ordersOfCustomerAverage, MidpointRounding.AwayFromZero),
                OrderPriceAverage = Math.Round(priceAverage, 2)
            };

            return Ok(report);
        }
    }
}