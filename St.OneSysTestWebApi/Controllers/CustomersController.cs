﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using St.OneSysTestWebApi.Controllers.Base;
using St.OneSysTestWebApi.DomainModel.DAL;
using St.OneSysTestWebApi.DomainModel.Models;
using St.OneSysTestWebApi.Models;

namespace St.OneSysTestWebApi.Controllers
{
    public class CustomersController : BaseApiController
    {
        public CustomersController()
        {
        }

        public CustomersController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IHttpActionResult Get()
        {
            var list = UnitOfWork.CustomerRepository.GetQuery()
                .Select(c => new CustomerApiModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    Email = c.Email
                })
                .ToList();

            return Ok(list);
        }

        public IHttpActionResult Get(int id)
        {
            var customer = UnitOfWork.CustomerRepository.GetQuery(c => c.Id == id)
                .Select(c => new CustomerApiModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    Email = c.Email
                })
                .ToList()
                .FirstOrDefault();

            if (customer == null)
            {
                return NotFound();
            }

            customer.Orders = UnitOfWork.OrderRepository.GetQuery(o => o.CustomerId == id)
                .Select(o => new OrderApiModel
                {
                    Price = o.Price,
                    CreatedDate = o.CreatedDate
                })
                .ToList();

            return Ok(customer);
        }

        public IHttpActionResult Post(int id, [FromBody] OrderApiModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            
            var customerExist = UnitOfWork.CustomerRepository.GetQuery(c => c.Id == id).Any();
            if (!customerExist)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newOrder = new Order
            {
                CustomerId = id,
                Price = model.Price,
                CreatedDate = model.CreatedDate ?? DateTime.Now
            };

            UnitOfWork.OrderRepository.Insert(newOrder);
            UnitOfWork.Save();

            return StatusCode(HttpStatusCode.Created);
        }

        public IHttpActionResult Post([FromBody] CustomerApiModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var newCustomer = new Customer
            {
                Name = model.Name,
                Email = model.Email
            };

            UnitOfWork.CustomerRepository.Insert(newCustomer);
            UnitOfWork.Save();

            return StatusCode(HttpStatusCode.Created);
        }


        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //public void Delete(int id)
        //{
        //}
    }
}