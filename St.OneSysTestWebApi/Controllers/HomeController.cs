﻿using System.Web;
using System.Web.Mvc;

namespace St.OneSysTestWebApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
