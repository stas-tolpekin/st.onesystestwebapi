﻿using System.Web.Http;
using St.OneSysTestWebApi.DomainModel.DAL;

namespace St.OneSysTestWebApi.Controllers.Base
{
    public class BaseApiController : ApiController
    {
        public BaseApiController()
        {
            UnitOfWork = new UnitOfWork();
        }

        public BaseApiController(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }
        
        protected IUnitOfWork UnitOfWork; 

    }
}