﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace St.OneSysTestWebApi.Models
{
    [DataContract(Name = "Customer")]
    public class CustomerApiModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [MaxLength(1000)]
        public string Name { get; set; }

        [DataMember]
        [Required(AllowEmptyStrings = false)]
        [MaxLength(2000)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<OrderApiModel> Orders { get; set; }
    }
}