using System;
using System.Runtime.Serialization;

namespace St.OneSysTestWebApi.Models
{
    [DataContract(Name = "Order")]
    public class OrderApiModel
    {
        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }
    }
}