﻿using System.Runtime.Serialization;
using St.OneSysTestWebApi.DomainModel.Models;


namespace St.OneSysTestWebApi.Models
{
    [DataContract(Name = "Report")]
    public class ReportApiModel
    {
        /// <summary>
        /// Количество записей <see cref="Customer"/>
        /// </summary>
        [DataMember]
        public int CustomerCount { get; set; }

        /// <summary>
        /// Количество записей <see cref="Order"/>
        /// </summary>
        [DataMember]
        public int OrderCount { get; set; }

        /// <summary>
        /// Среднее количество <see cref="Order"/> на одного <see cref="Customer"/>
        /// </summary>
        [DataMember]
        public double OrdersOfCustomerAverageCount { get; set; }

        /// <summary>
        /// Средний размер <see cref="Order.Price"/> по всем <see cref="Order"/>
        /// </summary>
        [DataMember]
        public decimal OrderPriceAverage { get; set; }
    }
}