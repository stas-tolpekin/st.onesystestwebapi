﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace St.OneSysTestWebApi.DomainModel.Models
{
    public class Customer : BaseEntity
    {

        [Display(Name = "Имя")]
        [Required(AllowEmptyStrings = false)]
        [MaxLength(1000)]
        public string Name { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false)]
        [MaxLength(2000)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Display(Name = "Заказы")]
        public ICollection<Order> Orders { get; set; } 

    }
}
