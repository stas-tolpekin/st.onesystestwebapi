﻿using System;
using System.ComponentModel.DataAnnotations;

namespace St.OneSysTestWebApi.DomainModel.Models
{
    public class Order : BaseEntity
    {

        [Display(Name = "Клиент")]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreatedDate { get; set; }

    }
}
