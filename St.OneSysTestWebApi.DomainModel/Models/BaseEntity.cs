﻿using System.ComponentModel.DataAnnotations;

namespace St.OneSysTestWebApi.DomainModel.Models
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
