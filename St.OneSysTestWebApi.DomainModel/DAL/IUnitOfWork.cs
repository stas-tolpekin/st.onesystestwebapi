﻿using St.OneSysTestWebApi.DomainModel.Models;

namespace St.OneSysTestWebApi.DomainModel.DAL
{
    public interface IUnitOfWork
    {
        IGenericRepository<Customer> CustomerRepository { get; }
        IGenericRepository<Order> OrderRepository { get; }

        /// <summary>
        /// Сохранение данных в хранилище
        /// </summary>
        void Save();
        
    }
}