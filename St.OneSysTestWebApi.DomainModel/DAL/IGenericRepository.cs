﻿using System;
using System.Linq;
using System.Linq.Expressions;
using St.OneSysTestWebApi.DomainModel.Models;

namespace St.OneSysTestWebApi.DomainModel.DAL
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity 
    {

        /// <summary>
        /// Вернуть объект <see cref="IQueryable"/> для доступа к данным
        /// </summary>
        /// <param name="filter">Выражение для фильтрации</param>
        /// <param name="orderBy">Выражение для сортировки</param>
        /// <param name="includeProperties">Список полей загружаемых с запросом. Через запятую без пробелов.</param>
        /// <returns>Объект запроса <see cref="IQueryable{TEntity}"/></returns>
        IQueryable<TEntity> GetQuery(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        
        /// <summary>
        /// Вставить сущность
        /// </summary>
        void Insert(TEntity entity);
        
        /// <summary>
        /// Удалить сущность
        /// </summary>
        void Delete(TEntity entityToDelete);

        /// <summary>
        /// Обновить существующую сущность
        /// </summary>
        void Update(TEntity entityToUpdate);
        
        
    }
}
