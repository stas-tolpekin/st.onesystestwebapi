﻿using St.OneSysTestWebApi.DomainModel.Models;

namespace St.OneSysTestWebApi.DomainModel.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork()
        {
            _context = new AppDbContext();
        }

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public UnitOfWork(string connectionString)
        {
            _context = new AppDbContext(connectionString);
        }

        private readonly AppDbContext _context;




        private IGenericRepository<Customer> _customerRepository;

        public IGenericRepository<Customer> CustomerRepository => _customerRepository ?? (_customerRepository = new GenericRepository<Customer>(_context));



        private IGenericRepository<Order> _orderRepository;
        public IGenericRepository<Order> OrderRepository => _orderRepository ?? (_orderRepository = new GenericRepository<Order>(_context));
        public void Save()
        {
            _context.SaveChanges();
        }

        
    }
}
